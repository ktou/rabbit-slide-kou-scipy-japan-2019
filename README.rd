= Apache Arrow - A cross-language development platform for in-memory data

Apache Arrow is the future for data processing systems. This talk
describes how to solve data sharing overhead in data processing system
such as Spark and PySpark. This talk also describes how to accelerate
computation against your large data by Apache Arrow.

== License

=== Slide

CC BY-SA 4.0

Use the followings for notation of the author:

  * Kouhei Sutou

==== ClearCode Inc. logo

CC BY-SA 4.0

Author: ClearCode Inc.

It is used in page header and some pages in the slide.

==== Performance figure

Apache License 2.0

Author: The Apache Software Foundation

== For author

=== Show

  rake

=== Publish

  rake publish

== For viewers

=== Install

  gem install rabbit-slide-kou-scipy-japan-2019

=== Show

  rabbit rabbit-slide-kou-scipy-japan-2019.gem


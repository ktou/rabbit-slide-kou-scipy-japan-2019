#!/usr/bin/env python3

import numpy as np
import pandas as pd
import pyarrow as pa
import pickle
import time
import timeit

data = {}
for i in range(100):
    data[str(i)] = np.random.randn(50000)
print(timeit.timeit(lambda: pd.DataFrame(data),
                    number=10))
df = pd.DataFrame(data)
print(timeit.timeit(lambda: pa.Table.from_pandas(df),
                    number=10))
table = pa.Table.from_pandas(df)
record_batch = pa.RecordBatch.from_pandas(df)

before = time.time()
# serialized = pa.ipc.serialize_pandas(df)
sink = pa.BufferOutputStream()
writer = pa.RecordBatchStreamWriter(sink, record_batch.schema)
writer.write_batch(record_batch)
writer.close()
serialized = sink.getvalue()
serialized_time = time.time() - before
print("serialize: {:.4f}s".format(serialized_time))

before = time.time()
# pa.ipc.deserialize_pandas(serialized)
buffer_reader = pa.BufferReader(serialized)
reader = pa.RecordBatchStreamReader(buffer_reader)
table = reader.read_all()
deserialized_time = time.time() - before
print("deserialize: {:.4f}s".format(deserialized_time))

before = time.time()
serialized = pickle.dumps(df)
serialized_time = time.time() - before
print("serialize: {:.4f}s".format(serialized_time))

before = time.time()
pickle.loads(serialized)
deserialized_time = time.time() - before
print("deserialize: {:.4f}s".format(deserialized_time))

